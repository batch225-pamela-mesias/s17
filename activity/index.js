/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

//first function here:

function personalInformation() {
	let yourFullName = prompt("Enter Your Full Name");
	let yourAge = prompt("Enter Your Age");
	let yourLocation = prompt("Enter Your Location");

	console.log("Hello, " + yourFullName);
	console.log("You are " + yourAge + " " + "years old.");
	console.log("You live in " + yourLocation);
}

personalInformation();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//second function here:
function myFavoriteArtists() {
	let theBeatles = "1. The Beatles";
	let theNirvana = "2. Nirvana";
	let theQueen = "3. Queen";
	let theOasis = "4. Oasis";
	let theLinkinPark = "5. Linkin Park";

	console.log(theBeatles);
	console.log(theNirvana);
	console.log(theQueen);
	console.log(theOasis);
	console.log(theLinkinPark);
}

myFavoriteArtists();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//third function here:
function myFavoritemovies() {
	let theAvengersEndgame = "1. Avengers: Endgame ";
	let theAvengersEndgameRate = "94%";

	let theGodFather = "2. The Godfather";
	let theGodFatherRate = "97%";

	let theShawshankRedemption = "3. Shawshank Redemption";
	let theShawshankRedemptionRate = "91%";

	let theKillOfMockingBird = "4. To Kill A Mockingbird";
	let theKillOfMockingBirdRate = "93%";

	let thePsycho = "5. Psycho";
	let thePsychoRate = "96%";

	console.log(theAvengersEndgame);
	console.log("Rotten Tomatoes Rating: " + theAvengersEndgameRate);

	console.log(theGodFather);
	console.log("Rotten Tomatoes Rating: " + theGodFatherRate);

	console.log(theShawshankRedemption);
	console.log("Rotten Tomatoes Rating: " + theShawshankRedemptionRate);

	console.log(theKillOfMockingBird);
	console.log("Rotten Tomatoes Rating: " + theKillOfMockingBirdRate);

	console.log(thePsycho);
	console.log("Rotten Tomatoes Rating: " + thePsychoRate);
}

myFavoritemovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function printUsers() {
	let friend1 = prompt("Enter your first friend's name:");
	let friend2 = prompt("Enter your second friend's name:");
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1);
	console.log(friend2);
	console.log(friend3);
}

printUsers();
